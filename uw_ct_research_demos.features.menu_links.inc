<?php
/**
 * @file
 * uw_ct_research_demos.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_research_demos_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_research-demos:research-demos.
  $menu_links['main-menu_research-demos:research-demos'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'research-demos',
    'router_path' => 'research-demos',
    'link_title' => 'Research Demos',
    'options' => array(
      'identifier' => 'main-menu_research-demos:research-demos',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Research Demos');

  return $menu_links;
}
